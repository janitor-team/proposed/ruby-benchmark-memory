module Benchmark
  module Memory
    Error = Class.new(StandardError)

    ConfigurationError = Class.new(Error)
  end
end
